<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Utility\Text;

/**
 * Users Model
 *
 */
class UsersTable extends Table
{

	/**
	 * Initialize method
	 *
	 * @param array $config The configuration for the Table.
	 * @return void
	 */
	public function initialize(array $config)
	{
		parent::initialize($config);

		$this->table('users');
		$this->displayField('id');
		$this->primaryKey('id');

		$this->addBehavior('Timestamp');
	}

	/**
	 * Default validation rules.
	 *
	 * @param \Cake\Validation\Validator $validator Validator instance.
	 * @return \Cake\Validation\Validator
	 */
	public function validationDefault(Validator $validator)
	{
		$validator
			->requirePresence('password', 'update')
			->notEmpty('password');

		
		return $validator;
	}

	public function getAdminRecord($field = 'id', $data) {
		$query = $this->find('all', [
			'conditions' => [
				'Users.'.$field => $data,
				'Users.id' => (int)true
			],

		]);
		if (!empty($query->hydrate('false')->toArray()[0])) {
			return $query->hydrate('false')->toArray()[0];
		}
		return false;
	}


	public function updateAdminRecord($fields, $data, $record) {
		$usersTable = TableRegistry::get('Users');
		$users = $usersTable->get($data); // Return users with id 1
		foreach ($fields as $value) {
			$users->$value = $record[$value];
		}
		if ($usersTable->save($users)) {
			return true;
		}
		return false;
	}



}
