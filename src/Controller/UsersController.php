<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Utility\Text;
use Cake\ORM\Table;
use Cake\ORM\TableRegistry;
/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class UsersController extends AppController
{

	/**
	 * Initialization hook method.
	 *
	 * Use this method to add common initialization code like loading components.
	 *
	 * e.g. `$this->loadComponent('Security');`
	 *
	 * @return void
	 */
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent('RequestHandler');
		$this->loadComponent('Flash');
		$this->Auth->allow(['logout', 'forgotPassword', 'resetPassword', 'updatePassword']);
	}

	public function login() {
		$this->set('title', 'CRM login');
		$this->viewBuilder()->layout('login');
		
		if ($this->Auth->user()){
			return $this->redirect($this->Auth->redirecturl());
		}
		if ($this->request->is('post')  && !empty($this->request->data('username'))) {
			$user = $this->Auth->identify();
			if ($user) {
				$this->Auth->setUser($user);
				$this->Flash->success(__('You Are successfully login'));
				return $this->redirect($this->Auth->redirecturl());
			} else {
				$message = __('Username or password is incorrect.');
				$this->_flashMessage($message, 'auth', 'error');
			}
		}
	}

	public function logout() {

		$message = __('You are now logged out.');
		$this->_flashMessage($message, 'auth', 'success');
		return $this->redirect($this->Auth->logout());
	}

	public function forgotPassword() {
		
		if ($this->request->is('post') && !empty($this->request->data('email'))) {
			$users = $this->Users->getAdminRecord('email', $this->request->data('email'));
			if (empty($users) || !$users) {
				
				$message = __('Email address not exists.');
				$this->_flashMessage($message, 'auth', 'error');
				return $this->redirect(array('action' => 'login'));
			}
			$uuid = Text::uuid();
			$updateStatus = $this->Users->updateAdminRecord(array('persist_code'), $users['id'], array('persist_code' => $uuid));
			
			if (!$updateStatus) {

				$message = __('Something went wrong. Please try again.');
				$this->_flashMessage($message, 'auth', 'error');
				return $this->redirect(array('action' => 'login'));
			}

			$data = array(
				'resetPasswordCode' => $uuid,
				'firstName' => $users['first_name'],
				'lastName' => $users['last_name']
			);
			
			$status = $this->_sendEmailMessage("karan.sharma@ucodesoft.com", 'forgotPassword', $data, 'Forgot Password');
			
			if ($status) {
				$message = __('Please check your email address.The link has been sent to your email address.');
				$this->_flashMessage($message, 'auth', 'success');
				return $this->redirect(array('action' => 'login'));
			}
		}
	}

	public function resetPassword($link) {
		$this->set('title', 'CRM Reset Password');
		$this->viewBuilder()->layout('login');
		$users = $this->Users->getAdminRecord('persist_code', $link);
		
		if (empty($users) || !$users) {
			$message = __('Something went wrong. Please try again.');
			$this->_flashMessage($message, 'auth', 'error');
			return $this->redirect(array('action' => 'login'));
		}

		$this->set('persist_code', $users['persist_code']);

	}

	public function updatePassword() {
		
		if ($this->request->is('post')  && !empty($this->request->data('password'))) {
			
			$users = $this->Users->getAdminRecord('persist_code', $this->request->data('persist_code'));
			if (empty($users) || !$users) {
				
				$message = __('Something went wrong. Please try again.');
				$this->_flashMessage($message, 'auth', 'error');
				return $this->redirect(array('action' => 'login'));
			}
			$updateStatus = $this->Users->updateAdminRecord(array('password', 'persist_code'), $users['id'], array('password' => $this->request->data('password'), 'persist_code' => null));
			if ($updateStatus) {
				$message = __('Password has been updated succesfully.');
				$this->_flashMessage($message, 'auth', 'success');
			} else {
				$message = __('Something went wrong. Please try again.');
				$this->_flashMessage($message, 'auth', 'error');
			}
		}
		return $this->redirect(array('action' => 'login'));	
	}

}
