<!-- <div class="message success" onclick="this.classList.add('hidden')"><?= h($message) ?></div> -->
<div class="alert alert-success">
				<a aria-label="close" data-dismiss="alert" class="close" href="#">×</a>
				<i class="glyphicon glyphicon-ok"></i> <strong>Success!</strong> 
				<div onclick="this.classList.add('hidden');" class="message success">
					<?= h($message) ?>
				</div>
</div>
