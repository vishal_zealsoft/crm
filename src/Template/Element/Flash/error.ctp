<!-- <div class="message error" onclick="this.classList.add('hidden');"></div> -->
<div class="alert alert-danger">
				<a aria-label="close" data-dismiss="alert" class="close" href="#">×</a>
				<i class="glyphicon glyphicon-remove-circle"></i> <strong>Error!</strong> 
				<div onclick="this.classList.add('hidden');" class="message error">
					<?= h($message) ?>
				</div>
</div>