<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title><?php echo $title; ?></title>
      <!-- Bootstrap -->
     	<?php echo $this->Html->css(array(
					'bootstrap.min.css',
					'login.css'
				)
			);
		?>
      <link href='https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="js/html5shiv.min.js"></script>
      <script src="js/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>

      <div class="site-wrapper">
      <?php echo $this->fetch('content'); ?>
      </div>
      <!-- Button trigger modal -->
      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	  	<?php echo $this->html->script(array(
				'jQuery-2.1.4.min',
				'bootstrap.min'
				)
			);
		?>
      <script type="text/javascript">
         $(".forget-link").click(function(){
             $(".login").addClass('hide');
             $(".forget-password").removeClass('hide');      
         });
         
          $(".log-in").click(function(){
             $(".login").removeClass('hide');
             $(".forget-password").addClass('hide');        
         });            
      </script>
   </body>
</html>

