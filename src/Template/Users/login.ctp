
 

<div class="site-wrapper-inner">
	<div class="login-layout-wraper">

   		<div class="col-center col-lg-5 col-xs-12 col-sm-10 col-md-10 signup-section clearfix">
		   <?php  echo $this->flash->render('auth'); ?>
		   
		  	<div class="right-section col-lg-12 col-xs-12 col-sm-12 col-md-12">
				<div class="login">
					<div class="text-center">
						<?php echo $this->html->image('David_logo.png', array('alt' => 'logo')) ?>
				   		
					</div>
					<h2 class="text-center login-heading">Welcome to our CRM</h2>
					<?php 
						echo $this->Form->create('User', array(
								'url' => array(
									'controller' => 'Users',
									'action' => 'login'
								),
								'class' => 'form-horizontal signup-form',
								'method' => 'post'
							)
						);
					 ?>
						<div class="form-group">
						  <div class="col-lg-12">
						  	<?php 
						  		echo $this->Form->input('username', array(
							  			'label' => false,
							  			'div' => false,
							  			'style' => false,
							  			'class' => 'form-control',
							  			'placeholder' => 'Username'
						  			)
						  		); 
						  	?>
							
						  </div>
					   	</div>
					   	<div class="form-group">
						  <div class="col-lg-12">
						  <?php 
						  	echo $this->Form->input('password', array(
						  				'type' => 'password',
						  				'label' => false,
						  				'div' => false,
						  				'class' => 'form-control',
						  				'id' => 'password',
						  				'placeholder' => 'Password'
						  			)
						  		);
						  	?>
						  </div>
					   	</div>
					   	<div class="form-group">
						  <div class="col-lg-12">
								<div class="text-right text-white forget-link">
							 		<a href="javascript:void(0)">Can't remember login Credentials?</a>
							 	</div>
						  </div>
					   	</div>
					   	<div class="form-group login-submit-btn">
						  	<div class="col-lg-12">
							  	<?php echo $this->Form->input('Log In', array(
							  			'type' => 'submit',
							  			'class' => 'btn btn-border-white  btn-block',
							  			'div' => false,
							  			'label' => false
							  			)
							  		);
							  	 ?>
							 <!-- <button class="btn btn-border-white  btn-block" type="button">Log In</button> -->
						  	</div>
					   	</div>
					<?php echo $this->Form->end(); ?>
				 </div>
			 	<div class="forget-password hide ">
					<div class="text-center">
				   		<?php echo $this->html->image('David_logo.png', array('alt' => 'logo')) ?>
					</div>
					<h2 class="text-center login-heading">Forget Password</h2>
					<?php 
						echo $this->Form->create('User', array(
								'url' => array(
									'controller' => 'Users',
									'action' => 'forgotPassword'
								),
								'class' => 'form-horizontal signup-form',
								'method' => 'post'
							)
						);
					 ?>
					<form class="form-horizontal signup-form">
				   		<p class="forget-text text-center">
				   			Enter your E-Mail address and we will send you the link to reset your password
				   		</p>
				   		<div class="form-group ">
					  		<div class="col-lg-12">
					  		<?php 
						  		echo $this->Form->input('email', array(
							  			'type' => 'email',
							  			'label' => false,
							  			'div' => false,
							  			'style' => false,
							  			'class' => 'form-control',
							  			'placeholder' => 'Email'
						  			)
						  		); 
						  	?>
						 		<!-- <input type="text" class="form-control" placeholder="Email"> -->
					  		</div>
				   		</div>
				   		<div class="form-group forget-btn">
					  		<div class="col-lg-6">
					  		<?php echo $this->Form->input('Request', array(
							  			'type' => 'submit',
							  			'class' => 'btn btn-border-white  btn-block',
							  			'div' => false,
							  			'label' => false
							  			)
							  		);
							  	 ?>
						 		<!-- <button class="btn btn-border-white btn-block" type="button">Request</button> -->
					  		</div>
					  		<div class="col-lg-6">
								<button class="btn btn-border-white log-in btn-block" type="button">Back to Log in</button>
					  		</div>
				   		</div>
					</form>
			 	</div>
		  	</div>
   		</div>
	</div>
</div>