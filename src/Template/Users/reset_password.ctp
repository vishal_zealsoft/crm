
 

<div class="site-wrapper-inner">
	<div class="login-layout-wraper">

   		<div class="col-center col-lg-5 col-xs-12 col-sm-10 col-md-10 signup-section clearfix">
		   <?php  echo $this->flash->render('auth'); ?>
		   
		  	<div class="right-section col-lg-12 col-xs-12 col-sm-12 col-md-12">
				<div class="login">
					<div class="text-center">
						<?php echo $this->html->image('David_logo.png', array('alt' => 'logo')) ?>
				   		
					</div>
					<h2 class="text-center login-heading">Welcome to our CRM</h2>
					<?php 
						echo $this->Form->create('User', array(
								'url' => array(
									'controller' => 'Users',
									'action' => 'updatePassword'
								),
								'class' => 'form-horizontal signup-form',
								'method' => 'post'
							)
						);

				  		echo $this->Form->input('persist_code', array(
					  			'type' => 'hidden',
					  			'value' => $persist_code
				  			)
				  		); 
						
					 ?>

						<div class="form-group">
						  <div class="col-lg-12">
						  	<?php 
						  		echo $this->Form->input('password', array(
							  			'type' => 'password',
							  			'label' => false,
							  			'div' => false,
							  			'style' => false,
							  			'class' => 'form-control',
							  			'placeholder' => 'Password'
						  			)
						  		); 
						  	?>
							
						  </div>
					   	</div>
					   	<div class="form-group">
						  <div class="col-lg-12">
						  <?php 
						  	echo $this->Form->input('cpassword', array(
						  				'type' => 'password',
						  				'label' => false,
						  				'div' => false,
						  				'class' => 'form-control',
						  				'id' => 'password',
						  				'placeholder' => 'Confirm Password'
						  			)
						  		);
						  	?>
						  </div>
					   	</div>
					   	<div class="form-group login-submit-btn">
						  	<div class="col-lg-12">
							  	<?php echo $this->Form->input('Update', array(
							  			'type' => 'submit',
							  			'class' => 'btn btn-border-white  btn-block',
							  			'div' => false,
							  			'label' => false
							  			)
							  		);
							  	 ?>
						  	</div>
					   	</div>
					<?php echo $this->Form->end(); ?>
				 </div>
		  	</div>
   		</div>
	</div>
</div>